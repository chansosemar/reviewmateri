// Ada Sebuah mobil berwarna merah melaju dengan kecepatan 250km/j
// Ada sebuah motor berwarna biru beroda dua melaju 125kpj

class Kendaraan {
    constructor(wheels, color, speed) {
        this.name   = this.constructor.name;
        this.wheels = wheels;
        this.color = color;
        this.speed = speed;
    }
    getname = () => console.log(`Jenis kendaraan ini ${this.name}`);
    description = () =>  console.log(`Deskripsi : Beroda ${this.wheels}, Berwarna ${this.color}, Melaju dengan kecepatan ${this.speed}.`);
}

class Mobil extends Kendaraan {
    constructor(wheels, color, speed){
        super(wheels,color,speed); 
    }
}
class Motor extends Kendaraan {
    constructor(wheels, color, speed){
        super(wheels,color,speed); 
    }
}
const mobil = new Mobil(4,"red","250km/jam");
mobil.getname();
mobil.description();

const motor = new Motor(2,"biru","125km/jam");
motor.getname();
motor.description();



// const fetch = require('node-fetch'); //<-- diaktifin kalo mau jalananin via node




function panggil (){
    fetch('http://dummy.restapiexample.com/api/v1/employees')
    .then(response => {
        if (!response.ok){
            throw Error('error')
        }
        return response.json();
    }).then(data => {
        console.log(data.data);
        const html = data.data.map(user => {
            return `
            <div class="user">
                <p>ID : ${user.id}</p>
                <p>Name : ${user.employee_name}</p>
                <p>Age : ${user.employee_age}</p>
                <p>Salary : ${user.employee_salary}</p>
            </div>`
        })
        .join('');
        console.log(html);
        document.querySelector('#tugas')
        .insertAdjacentHTML('afterbegin',html );
    }).catch(error => {
        console.log(error);
    })
}

panggil();